// Users
{
	"_id" : "user001",
	"firstName" : "John",
	"lastName" : "Smith",
	"email" : "johnsmith@email.com",
	"password" : "JohnSmith123",
	"isAdmin?" : false,
	"mobileNumber" : "09123456789",
	"dateTimeRegistered" : "2022-06-10T15:00:00.00Z"
	"orderID" : ["order001"]

}

// Orders
{
	"_id" : "order001",
	"userID" : "user001"
	"transactionDate" : "06-10-2022",
	"status": "For Delivery",
	"total": "2500",
	"dateTimeCreated" : "2022-06-10T15:00:00.00Z"
}

// Products
{
	"_id": "prod001",
	"prodName": "mouse",
	"prodDesc": "Wireless mouse for everyday use.",
	"price": "250",
	"stocks": 8,
	"isActive?": true,
	"SKU":"SKU001"
	"dateTimeAdded" : "2022-06-10T15:00:00.00Z"

}

{
	"_id": "prod002",
	"prodName": "Keyboard",
	"prodDesc": "Wireless mechanical keyboard for everyday use.",
	"price": "1000",
	"stocks": 13,
	"isActive?": true,
	"SKU":"SKU002"
	"dateTimeAdded" : "2022-06-10T15:00:00.00Z"
}

// Order Products
{
	"_id" : "inCart001",
	"orderID" : "order001",
	"productID":"prod001",
	"quantity":2,
	"price": 250,
	"subtotal" : 500
}

{
	"_id" : "inCart002",
	"orderID" : "order001",
	"productID":"prod002",
	"quantity":2,
	"price": 1000,
	"subtotal" : 2000
}